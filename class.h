class classeA
{

private:
  //Constructeur dans le private
  classeA();
public:
  //Adresse mémoire de ou du futur objet ClasseA
  static classeA *instance;

  //Méthode pour instancier ou recuperer l'adresse mémoire
  static classeA *getInstance();

  //Nombre contenu dans l'objet
  int nombre;

};
