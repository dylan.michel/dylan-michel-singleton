#include "class.h"
#include <iostream>

int main()
{

//Ceci montre que l'on ne peut pas utiliser un constructeur priver
//  classeA objet = classeA();


  //Initialisation d'un pointeur de l'instance sur une première variable
  classeA *premier = classeA::getInstance();

  //Initialisation d'un pointeur de l'instance sur une deuxieme variable
  classeA *deuxieme =classeA::getInstance();


  //On affiche le resulat nombre, de l'objet qui est pointé par premier
  std::cout << premier->nombre <<std::endl;

  //On affiche les adresses mémoires des deux pointeurs pour montrer qu'ils sont identiques.
  std::cout << premier << std::endl;
  std::cout << deuxieme << std::endl;
  
  return 0;
}
