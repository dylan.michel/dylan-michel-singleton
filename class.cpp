#include "./class.h"

//On met l'adresse à zero puisqu'elle n'est pas encore instancier
classeA *classeA::instance = 0;

//Initialisation de la classe
classeA::classeA()
{
  nombre=132;
}

//Méthode pour crée ou donnée l'adresse de l'objet
classeA* classeA::getInstance()
{
  //Si l'adresse = 0 alors on crée l'objet
  if(instance==0)
  {
    instance = new classeA();
  }
  //On retourne l'adresse de l'objet
  return instance;
}
